let posts = [];
let count = 1;

const postForm = document.querySelector('#form-add-post'),
	editForm = document.querySelector('#form-edit-post'),
	txtTitle = document.querySelector('#txt-title'),
	txtBody = document.querySelector('#txt-body'),
	postEntries = document.querySelector('#div-post-entries'),
	editId = document.querySelector('#txt-edit-id'),
	editTitle = document.querySelector('#txt-edit-title'),
	editBody = document.querySelector('#txt-edit-body');

postForm.addEventListener('submit', addPost);
editForm.addEventListener('submit', updatePost);

function addPost(event) {
	event.preventDefault();

	posts.push({
		id: count,
		title: txtTitle.value,
		body: txtBody.value
	})

	count++;

	showPosts(posts);
	alert('Post successfully added');
}

function showPosts(posts) {
	let postEntry = '';

	posts.forEach((post) => {
		postEntry += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>`
	})

	postEntries.innerHTML = postEntry;
}

function editPost(id) {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	editId.value = id;
	editTitle.value = title;
	editBody.value = body;
}

function updatePost(event) {
	event.preventDefault();

	for(let i=0; i<posts.length; i++) {
		if(posts[i].id.toString() === editId.value) {
			posts[i].title = editTitle.value;
			posts[i].body = editBody.value;

			showPosts(posts);
			alert('Post successfully added');

			break;
		}
	}
}

function deletePost(id) {
	for(let i=0; i<posts.length; i++) {
		if(posts[i].id.toString() === id) {
			posts.splice(i, 1);

			showPosts(posts);
			alert('Post successfully added');

			break;
		}
	}
}